## WebScraping TJA

### Requisitos:

- Docker
 
### Estrutura

A estrutura inicial do projeto é a seguinte:
```
.
├── docker
│   ├── app
│   │   ├── Dockerfile
│   │   └── requirements.txt
│   ├── COPY_TO_.env
│   ├── docker-compose.yml
│   └── nginx
│       ├── Dockerfile
│       └── nginx.conf
├── README.md
└── tja
    ├── django_project
    │   ├── assets
    │   ├── config
    │   ├── manage.py
    │   ├── package.json
    │   ├── package-lock.json
    │   ├── README.md
    │   ├── scrapy.cfg
    │   ├── templates
    │   ├── tja_app
    │   └── webpack.config.js
    ├── geckodriver
    └── scrapy_project
        ├── scrapy.cfg
        └── tja_crawler
```

### Passos Para Instalação

Dentro da pasta docker, copie o arquivo `COPY_TO.env` para `.env` e defina as variáveis de ambiente

```
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_DB=
DJANGO_SETTINGS_MODULE=
TJA_ENVIRONMENT=
TJA_DB_NAME=
TJA_DB_HOST=
TJA_DB_USER=
TJA_DB_PASSWORD=
TJA_DB_PORT=
TJA_CORS_ORIGIN_WHITELIST=

# It is possible to generate a Secret Key with the command below
# python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
DJANGO_SECRET_KEY=
```

Dentro da pasta docker execute o comando abaixo para criar as imagens necessárias. O comando vai fazer o download das dependências do projeto e criar o ambiente
  
`docker-compose build`

Em seguida levante os containers. Use a opção `-d` se quiser deixar este processo em background. Este comando vai fazer o download das demais imagens que forem necessárias:

`docker-compose up`
 
Este comando deve lenvantar o container. Você pode checar se ele está em pé acessando a home do projeto que neste momento mostrará alguns erros.

[http://localhost:1337/](http://localhost:1337/)

Dentro da pasta `django_project` crie o arquivo `webpack-stats.json`

No terminal, entre no container `tja_app` e acessa a pasta `django_project` para rodar as migrations iniciais:

`docker exec -it tja_app bash`

`cd django_project`

`python manage.py migrate`

É hora de criar um usuário admin

`python manage.py createsuperuser`

Em seguida instale as dependências de frontend

`npm install --no-bin-links`

Em seguida gere os arquivos do frontend

`npm run start`

* Caso ocorra um erro de permissão, execute o comando abaixo para tornar o webpack executável:

`chmod +x ./node_modules/webpack/bin/webpack.js`

Por fim, mova os arquivos estáticos para a página staticfiles com o comando do django:

`python manage.py collectstatic`

Caso faça alterações nos arquivos de frontend em `assets`, você pode gerar uma nova distribuição com

`npm run start`

Ou monitorar mudanças continuamente com 

`npm run watch`

## Raspagem

- Para raspar os dados, crie um arquivo `processos.txt` na pasta `scrapy_project` com os números dos processos que devem ser raspados, sendo um processo por linha. Por exemplo:

```
0067154-55.2010.8.02.0001
0000575-40.2014.8.02.0081
0000214-28.2011.8.02.0081
0717561-98.2019.8.02.0001
0716715-81.2019.8.02.0001
0725703-91.2019.8.02.0001
```

- Para raspar os procssos execute

`scrapy crawl processo`

  
Caso o processo não tenha sido raspado em menos de 24h, ele estará disponível para acesso na aplicação.

### Próximos passos e melhorias

Abaixo estão algumas sugestões para melhorar a estrutura, qualidade e performance do projeto:

- Utilizar imagem python alpine ao invés de slim e diminuir o número de pacotes instalados no build. 

- Usar PhantomJS (ou outro navegador headless) ao invés de Firefox como webdriver do selenium

- Adicionar testes autotatizados no SPA, no backend e nos crawlers
