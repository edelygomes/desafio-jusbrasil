# -*- coding: utf-8 -*-
from scrapy import Field
from scrapy import Item

class TjaCrawlerItem(Item):
    process_id = Field()
    classe = Field()
    area = Field()
    assunto = Field()
    data_distribuicao = Field()
    ultima_atualizacao = Field()
    juiz = Field()
    valor_acao = Field()
    partes = Field()
    movimentacoes = Field()

