# -*- coding: utf-8 -*-
import scrapy, sys
from bs4 import BeautifulSoup
from datetime import timedelta
from django.utils import timezone
from selenium import webdriver
from tja_app.models import Processo
from selenium.webdriver.firefox.options import Options

class ProcessoSpider(scrapy.Spider):
    
    name = "processo"

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.soup = ''    
        self.driver = ''   
   
    def start_requests(self):
        urls_processo = {'start_urls': [], 'processos': []}

        base_url = 'https://www2.tjal.jus.br/cpopg/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado={}&foroNumeroUnificado={}&dadosConsulta.valorConsultaNuUnificado={}&dadosConsulta.valorConsulta='
        
        with open("processos.txt", "rt") as f:
            for p in f.readlines():
                num_processo = p.strip()
                query = Processo.objects.filter(pk=num_processo)
                if(len(query)>0):
                    processo = query[0]
                    agora = timezone.now()
                    umdia = agora - timedelta(days=1) 
                    ultima_atualizacao = processo.ultima_atualizacao
                    if(ultima_atualizacao > umdia):
                        print("""============== Processo {} foi salvo em menos de 24 horas ==============""".format(num_processo), flush=True)
                        continue

                print("""============== {} ==============""".format(num_processo), flush=True)
                try:
                    array_processo = num_processo.split('.')
                    foro_unificado = array_processo[4]
                    ano_digito_unificado = array_processo[0]+'.'+array_processo[1]
                    url = base_url.format(ano_digito_unificado, foro_unificado, num_processo)
                    if url and num_processo:
                        urls_processo['start_urls'].append(url)
                        urls_processo['processos'].append(num_processo)
                except Exception as e:
                    print("""============== Processo {} não pôde ser acessado ==============""".format(num_processo.strip()), flush=True)

                    print(e, flush=True)
                    continue
        
        i = 0
        for url in urls_processo['start_urls']:
            i += 1
            yield scrapy.Request(url=url, callback=self.parse,
            meta={
                'handle_httpstatus_list': [302],
                'process_id': urls_processo['processos'][i-1]
            })

    def parse(self, response):
        if(response.status == 302):
            options = Options()
            options.headless = True 
            if(not isinstance(self.driver, webdriver.firefox.webdriver.WebDriver)):
                self.driver = webdriver.Firefox(options=options)
            self.driver.get(response.url)
            self.soup = BeautifulSoup(self.driver.page_source, 'lxml')
        elif(response.status == 200):
            self.soup = BeautifulSoup(response.body, 'lxml')
        else:
            return
        if(len(self.soup) > 0):
            item = {}
            item['process_id']= response.meta.get('process_id')
            item_finalizado = self.parse_info(item)
            return item_finalizado
        return

    def parse_info(self, item):  
             
        chaves_valor = [('classe', 'Classe:'),('area', 'Área:'),('assunto', 'Assunto:'),('juiz', 'Juiz:'),('valor_acao', 'Valor da ação:'), ('data_distribuicao', 'Distribuição:')]
        for dado in chaves_valor: 
            string_found = self.soup.find(string=dado[1])
            temp = ""
            if(string_found is not None):
                temp = string_found.find_parent('tr').get_text("", strip=True).partition(':')
            item[dado[0]] = temp[2] if string_found is not None else temp          

        if(item['process_id'] is not None):
           
            td_as_partes = self.soup.select('#tableTodasPartes tbody > tr')
            if(len(td_as_partes) > 0 ):
                partes = td_as_partes
            else:
                partes = self.soup.select('#tablePartesPrincipais tbody > tr')

            td_as_movimentacoes = self.soup.select('#tabelaTodasMovimentacoes > tr')
            if(len(td_as_movimentacoes) > 0 ):
                movimentacoes = td_as_movimentacoes
            else:
                movimentacoes = self.soup.select('#tabelaUltimasMovimentacoes > tr')

            item['partes'] = self.parseia_partes(partes)
            item['movimentacoes'] = self.parseia_movimentacoes(movimentacoes)

        return item
    
    def parseia_movimentacoes(self, trs):
        movimentacoes = {'dados': []}
        for tr in trs:
            dado = {'data': '', 'movimentacao': ''}
            tds = tr.find_all('td')
            dado['data'] = tds[0].get_text("", strip=True)
            dado['movimentacao'] = tds[2].get_text(" - ", strip=True)
            if(dado['data'] or dado['movimentacao']):
                movimentacoes['dados'].append(dado)
        return movimentacoes

    def parseia_partes(self, trs):
        partes = {'dados': []}
        for tr in trs:
            dict_text = {'tipo': '', 'nome': '', 'filhos': []}
            if(len(tr.find_all('br')) != 0):
                tds = tr.find_all('td')
                dict_text['tipo'] = tds[0].get_text("", strip=True).partition(":")[0]
                td_filhos = str(tds[1]).strip().replace('\n','').replace('\t','')[:-5]
                indice = td_filhos.rfind('<br/>')
                filhos = []
                while(indice != -1):     
                    atual = td_filhos[indice+5:-1]
                    td_filhos = td_filhos[0:indice]
                    parte_atual = atual.partition('</span>')
                    tipo = parte_atual[0].partition('<span class="mensagemExibindo">')
                    dict_text['filhos'].append({'tipo': tipo[2].strip()[0:-1], 'nome': parte_atual[2].strip()})
                    indice = td_filhos.rfind('<br/>')
                dict_text['nome'] = td_filhos.partition('>')[2].strip()
            else:
                tr_text = tr.get_text("", strip=True).partition(":")
                dict_text['tipo'] = tr_text[0]
                dict_text['nome'] = tr_text[2]
            partes['dados'].append(dict_text)

        return partes

    def closed(self, processo):
        if(isinstance(self.driver, webdriver.firefox.webdriver.WebDriver)):
            self.driver.quit()