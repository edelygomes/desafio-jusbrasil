# -*- coding: utf-8 -*-

from tja_app.models import Processo
from datetime import datetime

class TjaCrawlerPipeline(object):
    
    def process_item(self, item, spider):
        self.item = item
        self.trata_dados()
        self.salva_dados()

    def trata_dados(self):
        data_array = self.item['data_distribuicao'].partition("às")[0].strip().split('/')
        self.item['data_distribuicao'] = "{}-{}-{}".format(data_array[2], data_array[1], data_array[0])
        part = self.item['valor_acao'].replace('.','').replace(',','.')
        if('R$' in part):
            valor_acao = part.partition('R$')[2].strip()
            self.item['valor_acao'] = valor_acao
        else:
            self.item['valor_acao'] = part        
        self.item['ultima_atualizacao'] = datetime.now()

    def salva_dados(self):
        try:
            processo = Processo.objects.filter(pk=self.item['process_id'])
            
            if(len(processo) > 0):
                processo = processo[0]
                processo.classe = self.item['classe']
                processo.area = self.item['area']
                processo.assunto = self.item['assunto']
                processo.data_distribuicao = self.item['data_distribuicao']
                processo.juiz = self.item['juiz']
                processo.valor_acao = self.item['valor_acao']
                processo.partes = self.item['partes']
                processo.movimentacoes = self.item['movimentacoes']
                processo.ultima_atualizacao = self.item['ultima_atualizacao']
                processo.save()
                
            else:
                processo = Processo.objects.update_or_create(**self.item)
            print("""============== Processo {} salvo ==============""".format(self.item['process_id']), flush=True)
        except Exception as e:
            print("""============== Processo {} não pôde ser salvo ==============""".format(self.item['process_id']), flush=True)
            print(e, flush=True)
