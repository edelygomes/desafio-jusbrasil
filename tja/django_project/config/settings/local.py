import socket
from .base import *  
from django.conf import settings

ALLOWED_HOSTS += ['0.0.0.0', '127.0.0.1', 'localhost']

INSTALLED_APPS += ['debug_toolbar']

MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']

hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())

INTERNAL_IPS = [ip[:-1] + '1' for ip in ips] + ['127.0.0.1', '0.0.0.0', 'localhost']