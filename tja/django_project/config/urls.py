import os
from django.contrib import admin
from django.urls import path, include, re_path
from tja_app.urls import urlpatterns as urls
from .views import redirect_view
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(urls)),
    path('', TemplateView.as_view(template_name='front-end/index.html')),
    #re_path(r'^.*$', redirect_view),
]

environment = os.getenv('TJA_ENVIRONMENT', '')

if environment == 'local':
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
