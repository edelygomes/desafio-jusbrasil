import React, { useReducer } from "react";
import Processo from "./components/processo";
import HeaderForm from "./components/headerForm";
import update from "immutability-helper";

export const AppContext = React.createContext();

const initialState = {
  processo: "",
  loading: false
};

function reducer(state, action) {
  switch (action.type) {
    
    case "UPDATE_PROCESSO":

      let upd = { processo: { $set: action.data.processo }, loading: { $set: action.data.loading }, erro: { $set: action.data.erro }};

      return update(state, upd);
    default:
      return initialState;
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <HeaderForm />
      <Processo />
    </AppContext.Provider>
  );
}

export default App;
