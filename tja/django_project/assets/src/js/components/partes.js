import React from "react";

const partes = props => {
  let comp = "";

  if (props.pts) {
    let partes = props.pts.map((_, i) => {
      let children = "";

      if (_.filhos) {
        children = _.filhos.map((_, i) => {
          return (
            <li className="partes__body__item" key={i}>
              <div className="partes__body__item__nome">
                {_.nome ? _.nome.toLowerCase() : ""}
              </div>
              <div className="partes__body__item__cargo"> {_.tipo} </div>
            </li>
          );
        });
      }

      return (
        <React.Fragment key={i}>
          <li className="partes__body__item">
            <div className="partes__body__item__nome">
              {_.nome ? _.nome.toLowerCase() : ""}
            </div>
            <div className="partes__body__item__cargo"> {_.tipo} </div>
          </li>
          {children}
        </React.Fragment>
      );
    });

    comp = (
      <React.Fragment>
        <h3 className="partes__header">Partes envolvidas</h3>
        <ul className="partes__body">{partes}</ul>
      </React.Fragment>
    );
  }
  return <div className="partes">{comp}</div>;
};

export default partes;
