import React from "react";

const detalhes = props => {
  let comp = "";

  if (props.dts) {
    let detalhes = props.dts.map((_, i) => {
      return <li key={i} className="detalhes__body__item">{_}</li>;
    });

    comp = (
      <React.Fragment>
        <h3 className="detalhes__header">Detalhes</h3>
        <ul className="detalhes__body">{detalhes}</ul>
      </React.Fragment>
    );
  }
  return <div className="detalhes">{comp}</div>;
};

export default detalhes;
