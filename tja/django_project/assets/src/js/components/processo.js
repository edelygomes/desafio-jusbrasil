import React, { useContext } from "react";
import { AppContext } from "../App";
import Partes from "./partes";
import Detalhes from "./detalhes";
import Movimentacoes from "./movimentacoes";
import Loading from "./loading";
import Erro from './error';

const Processo = () => {
  const { state, dispatch } = useContext(AppContext);
  let mov,
    pts,
    dts,
    pid,
    data = "";
  let components = "";
  if (state.processo) {
    mov = state.processo["movimentacoes"]["dados"];
    pts = state.processo["partes"]["dados"];
    pid = state.processo["process_id"];
    data = new Date(state.processo["data_distribuicao"]);
    dts = [
      state.processo["assunto"],
      state.processo["classe"],
      state.processo["area"],
      state.processo["juiz"]
    ];
    components = (
      <React.Fragment>
        <main className="processo__main">
          <Movimentacoes mov={mov} />
        </main>
        <aside className="processo__aside">
          <Detalhes dts={dts} />
          <Partes pts={pts} />
        </aside>
      </React.Fragment>
    );
  }

  return (
    <div className="processo">
      <Loading load={state.loading} />
      {state.erro && <Erro/>}
      <div className="container container--processo">
        <div className="processo__header">
          {pid && (
            <h1 className="processo__header__title">
              Processo n. {pid} do TJA
            </h1>
          )}
          {data && (
            <p className="processo__header__data">
              Distribuído em {data.toLocaleDateString()}
            </p>
          )}
        </div>
      </div>
      <div className="container container--processo">{components}</div>
    </div>
  );
};

export default Processo;
