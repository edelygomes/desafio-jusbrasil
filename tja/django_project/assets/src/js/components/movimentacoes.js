import React from "react";

const movimentacoes = props => {
  let items = "";
  if (props.mov) {
    items = props.mov.map((_, i) => {
      return (
        <li className="movimentacoes__body__item" key={i}>
          <span className="movimentacoes__body__item__data"> {_.data}</span>
          <span className="movimentacoes__body__item__movimentacao">
            {_.movimentacao}
          </span>
        </li>
      );
    });
  }

  return (
    <div className="movimentacoes">
      <div className="movimentacoes__header">
        <h2 className="movimentacoes__header__title">Movimentações</h2>
      </div>
      <ul className="movimentacoes__body">{items}</ul>
    </div>
  );
};

export default movimentacoes;
