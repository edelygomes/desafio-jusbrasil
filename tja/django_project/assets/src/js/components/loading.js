import React from "react";

const loading = props => {
  let classes = props.load ? ["lds-ring", "lds-ring--loading"] : ["lds-ring"] ;
  return <div className={classes.join(" ")}><div></div><div></div><div></div><div></div></div>
};

export default loading;
