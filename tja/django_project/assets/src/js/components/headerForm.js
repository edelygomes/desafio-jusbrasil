import React, { useContext } from "react";
import { AppContext } from "../App";
import { useForm } from "react-hook-form";
import axios from "../axios";

const HeaderForm = () => {
  const { state, dispatch } = useContext(AppContext);

  const { handleSubmit, register, errors } = useForm();

  const updateProcesso = processo => {
    dispatch({ type: "UPDATE_PROCESSO", data: processo });
  };

  const onSubmit = values => {

    updateProcesso({loading: true, erro: false});
    axios
      .get("processo/" + values.processo)
      .then(response => {
        let resposta = response.data;
        resposta['loading'] = false;
        response['erro'] = false;
        updateProcesso(response.data);
      })
      .catch(error => {
        updateProcesso({ processo: "", erro: error, loading: false });
      });
  };

  const headerClasses = ["header", "header--home"];
  return (
    <header className={headerClasses.join(" ")}>
      <div className="container container--header">
        <h1 className="header__title">Buscar</h1>
        <p className="header__subtitle">
          Selecione um tribunal para listar os processos ou buscar pelo número
          unificado
        </p>
        <form
          onSubmit={handleSubmit(onSubmit)}
          method="Post"
          className="header__form"
        >
          <select
            name="tribunal"
            ref={register}
            className="header__form__item header__form__item--select"
          >
            <option value="tja">Tribunal de Justiça de Alagoas</option>
          </select>
          <input
            name="processo"
            ref={register({
              required: 'Insira o número de um processo.',
              pattern: {
                value: /[0-9]{7}\-[0-9]{2}\.[0-9]{4}\.[0-9]{1}\.[0-9]{2}\.[0-9]{4}/,
                message: "Número de processo inválido"
              },
            })}
            placeholder="0000000-00.0000.0.00.0000"
            className="header__form__item header__form__item--input"
          />    
          <button type="submit" className="header__form__submit">
            Buscar
          </button>
        </form>
        <div className="header__erros">{errors.processo && errors.processo.message}</div>
      </div>
    </header>
  );
};

export default HeaderForm;
