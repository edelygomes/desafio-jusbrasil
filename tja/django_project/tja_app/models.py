from django.contrib.postgres.fields import JSONField
from django.db import models

class Processo(models.Model):
    process_id = models.CharField(primary_key=True, max_length=200, verbose_name="Número do Processo", null=False)
    classe = models.CharField(max_length=200)
    area = models.CharField(max_length=200, verbose_name="Área")
    assunto = models.CharField(max_length=200)
    data_distribuicao = models.DateField(verbose_name="Data da Distribuição", auto_now=True)
    juiz = models.CharField(max_length=200)
    valor_acao = models.DecimalField(max_digits=25, decimal_places=2, verbose_name="Valor da Ação")
    partes = JSONField()
    movimentacoes = JSONField(verbose_name="Movimentações")
    ultima_atualizacao = models.DateTimeField(verbose_name="Última Atualização", auto_now=True)

    def __str__(self):
        return self.process_id