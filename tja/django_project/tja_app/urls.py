from django.urls import include, path
from rest_framework import routers
from . import views

app_name="TJA"

urlpatterns = [
    path('api/processo/<pk>', views.ProcessoViewSet.as_view(), name="processo")
]