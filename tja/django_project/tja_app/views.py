from .models import Processo
from .serializers import ProcessoSerializer
from django.http import HttpResponseRedirect
from rest_framework.response import Response
from rest_framework import authentication, permissions, viewsets, views
from django.core import management

class ProcessoViewSet(views.APIView):
    
    serializer_class = ProcessoSerializer
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None, *args, **kwargs):

        process_id = ''
        _ = request.path.split('/')

        if(len(_) == 4):
            process_id = _[3]

        queryset = Processo.objects.filter(process_id=process_id)

        if(request.is_ajax() and process_id is not None):
            if(len(queryset) > 0):
                processo = ProcessoSerializer(queryset[0])
                request.session['processo'] = processo.data
                return Response({'processo': processo.data})
            else:
                response = Response({'processo': ''})
                response.status_code = 404
                return response
        return HttpResponseRedirect('/')