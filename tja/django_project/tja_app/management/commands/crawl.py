from django.core.management.base import BaseCommand
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import sys

sys.path.insert(0, '/app/scrapy_project')

class Command(BaseCommand):
    help = "Scrap tribunal"

    def handle(self, *args, **options):
        process = CrawlerProcess(get_project_settings())
        process.crawl('processo')
        process.start()